import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//import des bibliothes Fontawesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faClockRotateLeft, faComment, faCommentSlash, faDisplay, faEyeSlash, faReplyAll, faTextSlash, faTrash } from "@fortawesome/free-solid-svg-icons";
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import { faPlus} from "@fortawesome/free-solid-svg-icons";
import { faEye, faTrashCan } from '@fortawesome/free-regular-svg-icons';

library.add(faThumbsUp, faPlus, faEyeSlash, faEye , faTrashCan, faTextSlash, faClockRotateLeft, faReplyAll, faTrash, faDisplay, faComment, faCommentSlash);

createApp(App).component("font-awesome-icon", FontAwesomeIcon).use(store).use(router).mount('#app')

