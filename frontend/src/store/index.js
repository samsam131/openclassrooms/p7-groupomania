//import { resolve } from 'core-js/fn/promise';
import 'es6-promise/auto'
import { createStore } from 'vuex'

const axios = require('axios')
const instance = axios.create({
  baseURL: 'http://localhost:3000/api/users'
})

//Stockage UserId dans le local storage pour connexion au démarage
let user = localStorage.getItem('user')
if (!user) {
  user = {
    userId: -1,
    token: '',
    isAdmin: false
  }
} else {
  try {
    user = JSON.parse(user)
    console.log(user);
    instance.defaults.headers.common['Authorization'] = user.token
    //instance.defaults.headers.common['Authorization'] = `Basic ${user.token}`
    instance.defaults.headers.common.Authorization = `Baerer ${user.token}`
  }
  catch (ex) {
    user = {
      userId: -1,
      token: '',
      isAdmin: false
    }
  }

}

export default createStore({

  state: {
    //Stocker le statut de connexion 
    status: '',
    user: user,
    //isAdmin : ''
  },

  getters: {
  },

  mutations: {
    //Changement du statut de connexion
    setStatus(state, status) {
      state.status = status
    },
    //Changement des données d'identification
    logUser(state, user) {
      instance.defaults.headers.common['Authorization'] = user.token
      //instance.defaults.headers.common['Authorization'] = `Basic ${user.token}`
      //instance.defaults.headers.common.Authorization = `Bearer ${user.token}`
      localStorage.setItem('user', JSON.stringify(user))
      state.user = user
    },
    userInfos (state, userInfos) {
      state.userInfos = userInfos;
    },
    logout(state) {
      state.user = {
        userId: -1,
        token: '',
        isAdmin: false
      }
      localStorage.removeItem('user')
    },
  },

  actions: {
    //Création d'un compte
    createAccount: ({ commit }, userInfos) => {
      commit('setStatus', 'loading')
      return new Promise((resolve, reject) => {
        commit;
        instance
          .post("/signup", userInfos, {
            headers:
            {
              "Content-Type": "multipart/form-data"
            }
          }
          )
          .then((response) => {
            commit('setStatus', 'created')
            resolve(response)
            alert("Compte enregistré");
          })
          .catch((error) => {
            commit('setStatus', 'error_create')
            reject(error)
            alert("Le compte n a pas pu être créé");
          });
      })
    },

    //Connexion d'un itilisateur
    login: ({ commit }, userInfos) => {
      commit('setStatus', 'loading')
      return new Promise((resolve, reject) => {
        instance
          .post("/login", userInfos)
          .then((response) => {
            commit('setStatus', 'login')
            commit('logUser', response.data)
            resolve(response)
            alert("Utilisateur connecté");
          })
          .catch((error) => {
            commit('setStatus', 'error_login')
            reject(error)
            alert("Connexion impossible");
          });
      })
    },
  },
  modules: {
  }
})
