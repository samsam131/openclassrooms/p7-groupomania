import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/network',
    name: 'HomeView',
    component: () => import(/*webpackChunkName : "network"*/ '../views/HomeView.vue')
  },
  {
    path: '/register',
    name: 'RegisterView',
    component: () => import(/*webpackChunkName : "register"*/ '../views/RegisterView.vue')
  },
  {
    path: '/login',
    name: 'LoginView',
    component: () => import(/*webpackChunkName : "login"*/ '../views/LoginView.vue')
  },
  {
    path: '/account',
    name: 'AccountView',
    component: () => import(/*webpackChunkName : "account"*/ '../views/AccountView.vue')
  },
  {
    path: '/publish',
    name: 'PublishView',
    component: () => import(/*webpackChunkName : "publish"*/ '../views/PublishView.vue')
  },
  {
    path: '/admin',
    name: 'AccountAdminView',
    component: () => import(/*webpackChunkName : "publish"*/ '../views/AccountAdminView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
