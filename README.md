# P7-Groupomania : un projet Express et Vue.js

Ce projet constitue un réseau social de l'entreprise Groupomania. Il permet à chaque utilisateur disposant d'un compte de poster des messages avec ou sans supportmultimédia (image, videos) et commenter les messages. Chaque utilisateur visualise le post les plus récents puis tous les post. et peut afficher les commentaires de chaque post. Les utilisateurs peuvent supprimer leurs commentaires, leurs posts ainsi que leur compte.


## La structure
Le projet est constitué de deux dossiers : 
-backend
-frontend

### le backend
installer node.js
Serveur créé avec expres.js et nodemon server
L'ORM Sequelize pour gérer l'interface avec la base de données

### la base de données
Il s'agit d'une base de données relationnelle sql

### le frontend
Utilisation du framework Vuejs (v3) avec VueCLI. Le projet fonctionne avec vuex (utilisation du store) et vue-router pour la navigation.
Les requêtes sont adressées au backend avec 'axios'. les dates sont mise en forme avec dayjs


## Installation
Clôner le dépôt git

### Installation de la base de données
- Télécharger, installer et configurer un serveur mysql (par exemple Homebrew). Déterminer les mots de passe de l'utilisateur root
Pour plus d'information : https://openclassrooms.com/fr/courses/6971126-implementez-vos-bases-de-donnees-relationnelles-avec-sql/7152681-installez-le-sgbd-mysql

- se connecter à mysql avec l'invite de commande : 

    mysql -u root -p

et saisir le mot de passe



### Installation du backend
Dans le dossier backend, installer npm et les dépendences avec la commande : 

    npm install

Puis, installer le serveur nodemon avec : 

    npm install -g nodemon

Créer un fichier .env à partir du fichier .env.exemple avec vos paramètres

Créer un base de données sql :

    sequelize db:create

Effectuer la migration de la base de données avec sequelize avec la commande : 

    sequelize db:migrate


### Installation du frontend
Dans le dossier frontend, installer les dependences avec la commande :

    npm install --save 

## Lancement de l'application

### Démarrer le frontend
Dans le dossier frontend, saisir la commande :

    npm run serve

Cliquer sur un des deux liens pour accéder à l'application

    - Local:   http://localhost:8080/ 
    - Network: http://192.168.1.8:8080/

### Démarrer le backend
Dans le dossier frontend, saisir la commande

    nodemon server

Affichage de : "Listening on port 3000"

## Utilisation de l'application

Il est necessaire de créer un comppte et de s'authentifier pour accéder au réseau social.
Les 5 dernières publication sont affichés dans la partie haute du réseau
Tous les posts sont ensuite affichés. L'utilisateur peut afficher ou masquer les commentaires.
Chaque utilisateur peut : 
    -créer un post avec ou sans support multimédia (image, vidéo ou audio)
    -commenter les posts existants
    -supprimer ses posts ou ses commentaires
    -son compte (elle entraine la suppression des posts et des commentaires)
Un administrateur peut également : 
    -supprimer des posts et des commentaires
    -supprimer des commptes


