const express = require('express')
const router = express.Router()
const postCtrl = require('../controllers/post')
const multer = require('../middleware/multer-config')

router.post('/posts/create', multer, postCtrl.createPost )

router.get('/posts', multer, postCtrl.getAllPosts )
router.delete('/posts/delete/:id', multer, postCtrl.deletePost)
router.get('/posts/:id', postCtrl.getOnePost)
router.get('/latestposts/', postCtrl.getLatest)

module.exports = router