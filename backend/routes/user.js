const express = require('express')
const router = express.Router()
const userCtrl = require('../controllers/user')
const multer = require('../middleware/multer-config')


router.post('/users/signup', multer, userCtrl.signup) //TEST OK
router.post('/users/login', multer, userCtrl.login) //TEST OK
router.get('/users/one/:id', multer, userCtrl.getUserProfile) //TEST KO Wrong token (normal pas d'uthentification)
router.delete('/users/delete/:id', multer, userCtrl.deleteAccount)
router.delete('/users/delete/admin/:id', multer, userCtrl.deleteAccountAdmin)
router.get('/users/:id', multer, userCtrl.getAccount) //TEST OK
router.get('/users', multer, userCtrl.getAllAccounts) //TEST OK

module.exports = router