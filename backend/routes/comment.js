const express = require('express')
const router = express.Router()
const commentCtrl = require('../controllers/comment')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer-config')


router.post('/comments/create', commentCtrl.createComment )
router.get('/comments', commentCtrl.getAllComments )
router.delete('/comments/delete/:id', multer, commentCtrl.deleteComment)
router.get('/comments/:id', commentCtrl.getOneComment)

module.exports = router