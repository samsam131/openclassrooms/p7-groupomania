require('dotenv').config()

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const jwtUtils = require('../utils/jwt.utils')
const models = require('../models')
const fs = require('fs')
const JWT_SIGN_SECRET = process.env.APP_JWT_SIGN_SECRET

//Création d'un utilisateur
exports.signup = (req, res, next) => {

    //Définistion des constantes
    const email = req.body.email
    const password = req.body.password
    const lastName = req.body.lastName
    const firstName = req.body.firstName
    //Vérification de l'existance d'un fichier
    let urlImage = ""
    if (req.file) {
        urlImage = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } else {
        urlImage = null
    }

    //Vérifications des données saisies
    if (email == "" || lastName == "" || firstName == "" || password == "") {
        return res.status(400).json({
            error: 'Paramètre manquants'
        })
    } else {
        //Vérification si email déjà connu en BDD
        models.User.findOne({
            where: { email: email }
        })
            .then(user => {
                if (user) {
                    return res.status(401).json({
                        error: 'email déjà enregistré'
                    })
                } else {
                    bcrypt.hash(req.body.password, 10)
                        .then(hash => {

                            //Création de l'utilisateur en BDD avec photo
                            models.User.create({
                                email: email,
                                password: hash,
                                firstName: firstName,
                                lastName: lastName,
                                urlImage: urlImage,
                                isAdmin: false
                            })
                                .then(() => res.status(201).json({
                                    message: 'Utilisateur créé'
                                }))
                                .catch(error => res.status(400).json({
                                    error: 'Création impossible'
                                }))
                        })
                        .catch(error => res.status(500).json({
                            error
                        }))
                }
            })
            .catch(error => res.status(500).json({
                error
            }))
    }
}

//Connexion d'un utilisateur existant
exports.login = (req, res, next) => {

    //Définistion des contantes
    const email = req.body.email
    const password = req.body.password

    //Vérification si l'utilisateur existe en BDD
    models.User.findOne({
        where: {
            email: email
        }
    })
        .then(user => {
            if (!user) {
                return res.status(401).json({
                    error: 'Utilisateur ou mot de passe incorrect'
                })
            }
            //Comparaison du mots de passe et du hash en BDD
            bcrypt.compare(password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(402).json({
                            error: 'Utilisateur ou mot de passe incorrect'
                        })
                    }
                    res.status(200).json({
                        userId: user.id,
                        isAdmin: user.isAdmin,
                        token: jwt.sign({
                            userId: user.id,
                            isAdmin: user.isAdmin,
                        },
                            JWT_SIGN_SECRET, {
                            expiresIn: '12h'
                        })
                    })
                })
                .catch(error => res.status(500).json({
                    error
                }))
        })
        .catch(error => res.status(500).json({
            error
        }))
}

//Suppression d'un utilisateur par l'utilisateur
exports.deleteAccount = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization']
    var userId = jwtUtils.getUserId(headerAuth);

    //Vérifier si l'utilisateur existe déjà
    models.User.findOne({
        where: { id: userId }
    })
        .then(function (user) {
            if (user) {

                //Suppression utilisateur
                if (user.urlImage) {
                    const fileName = user.urlImage.split('/images/')[1]
                    fs.unlink(`images/${fileName}`, () => null)
                }
                models.User.destroy({
                    where: { id: user.id }
                })
                    .then(() => { res.status(201).json({ message: 'Compte supprimé' }) })
                    .catch(error => res.status(400).json({ error }))

            } else {
                res.status(404).json({ 'error': 'utilisateur non trouvé' });
            }
        }).catch(function (err) {
            res.status(500).json({ 'error': 'impossible de récupérer l utilisateur' });
        });
}

//Suppression d'un utilisateur par l'administrateur
exports.deleteAccountAdmin = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization']
    var userId = jwtUtils.getUserId(headerAuth);
    var accountId = req.params.id
    let isUserAdmin = jwtUtils.getIsAdmin(headerAuth);

    if (isUserAdmin == true) {
        models.User.findOne({
            where: { id: accountId }
        })
            .then(function (user) {
                if (user) {
                    //Suppression utilisateur
                    if (user.urlImage) {
                        const fileName = user.urlImage.split('/images/')[1]
                        fs.unlink(`images/${fileName}`, () => null)
                    }
                    user.destroy()
                        .then(() => {
                             models.User.findAll()
                            .then(users => {
                               res.status(200).json({ users })  
                            } )
                            
                            .catch(error => res.status(400).json({
                                error
                            }))
                        })
                        .catch(error => res.status(400).json({ error }))

                } else {
                    res.status(404).json({ 'error': 'utilisateur non trouvé' });
                }
            }).catch(function (err) {
                res.status(500).json({ 'error': 'impossible de récupérer l utilisateur' });
            });

    } else {
        res.status(403).json({ 'error': 'Requête non autorisée' });
    }



}
//Récupération d'un compte utilisateur 1
exports.getUserProfile = (req, res, next) => {

    //Recherche du compte en BDD
    models.User.findOne({
        attributes: ['id', 'email', 'lastName', 'firstName'],
        where: { id: req.params.id }
    })
        .then(function (user) {
            if (user) {
                res.status(201).json(user);
            } else {
                res.status(404).json({ 'error': 'user not found' });
            }
        })
        .catch(function (err) {
            res.status(500).json({ 'error': 'cannot fetch user' });
        });
}

//Récupération d'un compte utilisateur 2
exports.getAccount = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization'];
    var userId = jwtUtils.getUserId(headerAuth);

    //Authentification de la requête
    if (userId < 0)
        return res.status(400).json({ 'error': 'wrong token' });

    models.User.findOne({
        where: {
            id: req.params.id
        }
    })
        .then(user => res.status(200).json(user))
        .catch(error => res.status(404).json({ error }))
}

//Récupération de tous les comptes utilisateurs
exports.getAllAccounts = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization'];
    var userId = jwtUtils.getUserId(headerAuth);

    //Authentification de la requête
    if (userId < 0)
        return res.status(400).json({ 'error': 'wrong token' });

    models.User.findAll()
        .then(users => res.status(200).json(users))
        .catch(error => res.status(404).json({ error }))
}
