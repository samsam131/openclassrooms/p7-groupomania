const models = require('../models')
const fs = require('fs')
const jwtUtils = require('../utils/jwt.utils')
const descriptionMinlength = 4;
const descriptionMaxlength = 255;


//Création d'un commentaire

exports.createComment = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization']
    var userId = jwtUtils.getUserId(headerAuth);

    // Définitions des variables
    const attachment = req.body.attachment;
    const description = req.body.description;
    const postId = req.body.PostId

    //Vérification des données saisies
    if (description == "") {
        return res.status(400).json({ 'error': 'Paramètres manquants' });
    }
    if (description.length <= descriptionMinlength || description.length >= descriptionMaxlength) {
        return res.status(400).json({ 'error': 'Paramètres invalides' });
    }

    models.User.findOne({
        //attributes: [ 'id', 'email', 'lastName', 'firstName' ],
        where: { id: userId }
    })
        //Faire une recherche du post en BDD ?
        .then(function (user) {
            if (user) {
                //Création du commentaire
                models.Comment.create(
                    {
                        description: description,
                        attachment: attachment,
                        UserId: user.id,
                        PostId: postId
                    })
                    .then(() => {
                        models.Comment.findAll({
                            where: { postId: postId }
                        })
                            .then(comments => res.status(200).json({ comments }))
                            .catch(error => res.status(400).json({
                                error
                            }))
                    })
                    .catch(error => res.status(500).json({
                        error
                    }))
                    .catch(error => res.status(400).json({
                        error
                    }))
            } else {
                res.status(404).json({ 'error': 'user not found' });
            }
        })
        .catch(function (err) {
            res.status(500).json({ 'error': 'cannot fetch user' });
        });
}

//Suppression d'un commentaire
exports.deleteComment = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization']
    var userId = jwtUtils.getUserId(headerAuth);
    const commentId = req.params.id

    models.User.findOne({
        where: {
            id: userId
        }
    })
        .then((user) => {
            models.Comment.findOne({
                where: {
                    id: commentId
                }
            })
                .then((comment) => {
                    models.Post.findOne({
                        where: {
                            id: comment.PostId
                        }
                    })
                        .then((post) => {
                            if (!comment) {
                                res.status(404).json({
                                    error: 'Commentaire nontrouvé'
                                })
                            } else if (!user.isAdmin && comment.userId !== userId) {
                                res.status(403).json({ error: 'Requête non autorisée' })
                            } else {
                                models.Comment.destroy({
                                    where: {
                                        id: commentId
                                    }
                                })
                                    .then(() => {
                                        models.Comment.findAll({
                                            where: { postId: post.id },
                                            include: [{
                                                model: models.User
                                            }]
                                        })
                                            .then(comments => res.status(200).json({ comments }))
                                            .catch(error => res.status(400).json({
                                                error
                                            }))
                                    })
                                    .catch(error => res.status(500).json({
                                        error
                                    }))
                            }
                        })
                        .catch(error => {
                            res.status(404).json({ error: 'Post non trouvé' })
                        })
                })
                .catch(error => res.status(501).json({
                    error: 'Erreur requête'
                }))
        })
        .catch(error => res.status(501).json({
            error: 'Utilisateur non trouvé'
        }))
}

//Récupération de tous les commentaires
exports.getAllComments = (req, res, next) => {
    models.Comment.findAll()
        .then(comments => res.status(200).json(comments))
        .catch(error => res.status(500).json({ error }))
}

//Récupération d'un commentaire
exports.getOneComment = (req, res, next) => {
    models.Comment.findOne({
        where: {
            id: req.params.id
        }
    })
        .then(comment => res.status(200).json(comment))
        .catch(error => res.status(500).json({ error }))
}