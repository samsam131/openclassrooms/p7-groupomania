const models = require('../models')
const fs = require('fs')
const jwtUtils = require('../utils/jwt.utils')
const titleMinlength = 2;
const descriptionMinlength = 4;
const titleMaxlength = 25;
const descriptionMaxlength = 255;

//Créer un post
exports.createPost = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization']
    var userId = jwtUtils.getUserId(headerAuth);

    // Définitions des constantes
    const title = req.body.title;
    const description = req.body.description;
    //const attachment = req.body.attachment

    //Vérification de l'existance d'un fichier
    let attachment = ""
    if (req.file) {
        attachment = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } else {
        attachment = null
    }

    //Vérification des données
    if (title == "" || description == "") {
        return res.status(400).json({ 'error': 'Paramètres manquants' });
    }
    if (title.length <= titleMinlength || description.length <= descriptionMinlength || title.length >= titleMaxlength || description.length >= descriptionMaxlength) {
        return res.status(400).json({ 'error': 'Paramètres invalides' });
    }

    //Recherche de l'utilisateur en BDD
    models.User.findOne({
        //attributes: ['id', 'email', 'lastName', 'firstName'],
        where: { id: userId }
    })
        .then(function (user) {
            if (user) {

                //Création du Post avec fichier
                models.Post.create(
                    {
                        title: title,
                        description: description,
                        attachment: attachment,
                        UserId: user.id 
                    },
                )
                    .then(() => {
                        res.status(201).json({
                            message: 'Post créé',
                        })
                    }
                    )
                    .catch(error => res.status(400).json({
                        error
                    }))
            } else {
                res.status(404).json({ 'error': 'user not found' });
            }
        }).catch(function (err) {
            res.status(500).json({ 'error': 'cannot fetch user' });
        });
}

//Obtenir tous les posts
exports.getAllPosts = (req, res, next) => {


    models.Post.findAll({
        order: [['createdAt', 'DESC']],
        include: [{
            model: models.Comment,
            include: [{ model: models.User }]
        }],
        
    
    })
        .then(posts => res.status(201).json(posts))
        .catch(error => res.status(400).json({
            error
        }))
}

//Obetnir les 5 posts les plus récents
//Obtenir tous les posts
exports.getLatest = (req, res, next) => {
    const fields = req.query.fields
    const limit = parseInt(req.query.limit)
    const offset = parseInt(req.query.offset)
    const order = req.query.order

    models.Post.findAll(
        {
            order: [['createdAt', 'DESC']],
            limit: 5
        }) // or ASC
        .then(trends => res.status(201).json(trends))
        .catch(error => res.status(400).json({
            error
        }))
}

//Obtenir 1 seul post
exports.getOnePost = (req, res, next) => {
    models.Post.findOne({
        where: {
            id: req.params.id
        }
    })
        .then(post => res.status(200).json(post))
        .catch(error => res.status(500).json({ error }))
}

//Supprmier un post
exports.deletePost = (req, res, next) => {

    // Getting auth header -- Obtenir l'en-tête d'authentification
    var headerAuth = req.headers['authorization']
    var userId = jwtUtils.getUserId(headerAuth);

    models.User.findOne({
        where: {
            id: userId
        }
    })
        .then((user) => {
            models.Post.findOne({
                where: {
                    id: req.params.id
                }
            })
                .then((post) => {
                    if (!post) {
                        res.status(404).json({
                            error: 'Post nontrouvé'
                        })
                    } else if (!user.isAdmin && post.UserId !== userId) { //ça ne marche pas 
                        res.status(403).json({ error: 'Requête non autorisée' })
                    } else {
                        if (post.attachment) {
                            const fileName = post.attachment.split('/images/')[1]
                            fs.unlink(`images/${fileName}`, () => null)//fonction null obligatoire
                        }

                        models.Post.destroy({
                            where: {
                                id: req.params.id
                            }
                        })
                            .then(() => {
                                models.Post.findAll({
                                    include: [{
                                        model: models.Comment
                                    }],
                                })
                                    .then(posts => res.status(200).json({ posts }))
                                    .catch(error => res.status(400).json({
                                        error
                                    }))
                            })
                            .catch(error => res.status(500).json({
                                error
                            }))

                    }

                })
                .catch(error => res.status(501).json({
                    error
                }))
        })
        .catch(error => res.status(501).json({
            error: 'Utilisateur non trouvé'
        }))

}
