const express = require('express')
const userRoutes = require('./routes/user')
const postRoutes = require('./routes/post')
const commentRoutes = require('./routes/comment')
const app = express()
const path = require('path')
const sequelize = require('sequelize')

app.use(express.json())

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  next();
})

app.use('/api', userRoutes)
app.use('/api', postRoutes)
app.use('/api', commentRoutes)
app.use('/images', express.static(path.join(__dirname, 'images')));

module.exports = app