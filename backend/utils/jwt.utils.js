// Imports
require('dotenv').config()
var jwt = require('jsonwebtoken');
const JWT_SIGN_SECRET = process.env.APP_JWT_SIGN_SECRET

// Exported functions
module.exports = {
  generateTokenForUser: function(userData) {
    return jwt.sign({
      userId: userData.id,
      isAdmin: userData.isAdmin
    },
    JWT_SIGN_SECRET,
    {
      expiresIn: '12h'
    })
  },
  parseAuthorization: function(authorization) {
    return (authorization != null) ? authorization.replace('Basic ', '') : null;
  },
  getUserId: function(authorization) {
    var userId = -1;
    var token = module.exports.parseAuthorization(authorization);
    if(token != null) {
      try {
        var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
        if(jwtToken != null)
          userId = jwtToken.userId;
      } catch(err) {
      }
    }
    return userId;
  },
  getIsAdmin: function(authorization) {
    var isUserAdmin = false;
    var token = module.exports.parseAuthorization(authorization);
    if(token != null) {
      try {
        var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
        if(jwtToken != null)
        console.log(jwtToken, jwtToken.isAdmin);
          isUserAdmin = jwtToken.isAdmin;
      } catch(err) {
      }
    }
    return isUserAdmin;
  },
}